﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeEscape 
{
    class Wall : Sprite
    {
        //-------------------------
        //data
        //-------------------------

        //-------------------------
        //behaviour
        //-------------------------

        public Wall(Texture2D newTexture) : base(newTexture)//passes the informaitonup to the parent class(base class. Sprite
        {

        }
    
        public Vector2 GetCollisionDepth(Rectangle otherBounds)
        {
            Rectangle tileBounds = GetBound();
            //caclulate the half sizes of bothr rectangles
            float halfwidthPlayer = otherBounds.Width / 2.0f;
            float halfHeightPlayer = otherBounds.Height / 2.0f;
            float halfwidthTile = tileBounds.Width / 2.0f;
            float halfHeightTile = tileBounds.Height / 2.0f;

            //calculate the centers of each rectangle
            Vector2 centrePlayer = new Vector2(otherBounds.Left + halfwidthPlayer, otherBounds.Top + halfHeightPlayer);
            Vector2 centreTile = new Vector2(tileBounds.Left + halfwidthTile, tileBounds.Top + halfHeightTile);


            //distance between the centres
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            //minimum distance these need to be to NOT collide/intersect
            //if either x or the Y distance is gretaer than these minima, these are not itnersecting
            float minDistanceX = halfwidthPlayer + halfwidthTile;
            float mindistanceY = halfHeightPlayer + halfHeightTile;

            //if we are not intersecting at all return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= mindistanceY)
            {
                return Vector2.Zero;
            }

            //calculate and return intersection depth
            //essentially how much over the minimum intersection distance are we in each direction
            //aka by how much are they intersecting in taht direction
            float depthX = 0;
            float depthY = 0;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;

            if (distanceY > 0)
                depthY = mindistanceY - distanceY;
            else
                depthY = -mindistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }
       
    }
}
