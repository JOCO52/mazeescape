﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;


namespace MazeEscape
{
    class AnimatingSprite : Sprite
    {
        //----------------------
        //Types
        //----------------------

        struct Animation
        {
            public int startFrame;
            public int endFrame;
        }

        //--------------------------
        //Data
        //--------------------------

        //setting
        private int frameWidth;
        private int frameHeight;
        private float framesPerSecond ;
        private float timeInFrame;
        private Dictionary<string, Animation> animations = new Dictionary<string, Animation>();

        //Runtime
        private int currentFrame;
        private string currentAimation;
        private bool playing = false;


      


        //--------------------------
        //BEHAVIOUR
        //--------------------------

        public AnimatingSprite(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float framesPS) : base(newTexture)//passes the informaitonup to the parent class(base class. Sprite
        {
            frameWidth = newFrameWidth;
            frameHeight = newFrameHeight;
            framesPerSecond = framesPS;
        }

        public void AddAnimation(String name, int startFrame, int endFrame)
        {
            Animation newAnimation = new Animation();
            newAnimation.startFrame = startFrame;
            newAnimation.endFrame = endFrame;
            animations.Add(name, newAnimation);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                //draw animating sprite

                int numFramesX = texture.Width / frameWidth;

                int XframeIndex = currentFrame % numFramesX; //the rremainder when dividing
                int YframeIndex = currentFrame / numFramesX; //rounds down ignores the remainder

                Rectangle source = new Rectangle(XframeIndex * frameWidth, YframeIndex * frameHeight, frameWidth, frameHeight);

                spriteBatch.Draw(texture, position, source, Color.White);
            }
        }
        public virtual void Update(GameTime gametime)
        {
            //dont update if we arent actually playing
            if (!playing)
                return;

            float frametime = (float)gametime.ElapsedGameTime.TotalSeconds;

            //add to how long we hae been in this frame
            timeInFrame += frametime;

            //determine if it is time for a new frame
            //calculate how long a frame should last
            float timePerFrame = 1.0f / framesPerSecond;
            if (timeInFrame >= timePerFrame)
            {
                ++ currentFrame;
                timeInFrame = 0;

                //determine if we should reset current animation
                Animation thisAnimation = animations[currentAimation];

                if (currentFrame > thisAnimation.endFrame)
                {
                    //reset our animation back to the beginning frame
                    currentFrame = thisAnimation.startFrame;
                }
            }
        }

        public void playAnimation(string name)
        {
            if (name == currentAimation && playing == true)
                return;

            //error chcking, is this aimation even in the dictionary
            bool animationIsSetup = animations.ContainsKey(name);
            Debug.Assert(animationIsSetup, "animation Named" +name + " Not found in Animating sprite");

            //only change animation if the chosen animation is setup
            if (animationIsSetup)
            {
                //record the name of the curent animation
                currentAimation = name;
                //set the current frame to the first frame of our curretn animation
                currentFrame = animations[name].startFrame;
                //we just reset and started  amnew frame, so set frame to 0
                timeInFrame = 0;
                //start actually playing the animation
                playing = true;
            }
        }

        public void StopAnimation()
        {
            playing = false;

            //reset to the first frame of the animation
            currentFrame = animations[currentAimation].startFrame;
        }

        public override Rectangle GetBound()
        {
            return new Rectangle((int)position.X, (int)position.Y, frameWidth, frameHeight);
        }

    }
}
