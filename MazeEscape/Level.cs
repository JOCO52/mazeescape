﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace MazeEscape
{
    class Level
    {
        // ------------------
        // Data
        // ------------------
        Player player;
        Wall[,] walls;
        List<Coin> coins = new List<Coin>();
        int tileWidth;
        int tileHeight;
        int levelWidth;
        int levelHeight;
        int currentLevel;
        Text scoreDisplay;
        Text LifeDisplay;
        List<Spike> spikes = new List<Spike>();
        private bool reloadLevel = false;

        // Assets
        Texture2D playerSprite;
        Texture2D wallSprite;
        Texture2D coinSprite;
        Texture2D spikeSprite;
        SpriteFont UIFont;

        // ------------------
        // Behaviour
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            playerSprite = content.Load<Texture2D>("graphics/PlayerAnimation");
            wallSprite = content.Load<Texture2D>("graphics/Wall");
            coinSprite = content.Load <Texture2D>("graphics/coin");
            spikeSprite = content.Load<Texture2D>("graphics/spike");
            tileWidth = wallSprite.Width;
            tileHeight = wallSprite.Height;

            //set up UI
            UIFont = content.Load<SpriteFont>("fonts/mainFont");
            scoreDisplay = new Text(UIFont);
            scoreDisplay.SetPosition(new Vector2(10, 10));
            LifeDisplay = new Text(UIFont);
            LifeDisplay.SetPosition(new Vector2(graphics.Viewport.Bounds.Width -10, 10));
            LifeDisplay.SetAlignment(Text.Alignment.TOP_RIGHT);

            player = new Player(playerSprite, tileWidth, tileHeight, 6, this);

            //Temp
            LoadLevel(1);

            
        }
        // ------------------
        private void PositionPlayer(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            player.SetPosition(tilePosition);
        }
        // ------------------
        private void CreateWall(int tileX, int tileY)
        {
            Wall wall = new Wall(wallSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            wall.SetPosition(tilePosition);
            walls[tileX, tileY] = wall;
        }

        private void CreateCoin(int tileX, int tileY)
        {
            Coin coin = new Coin(coinSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            coin.SetPosition(tilePosition);
            coins.Add(coin);
        }

        private void CreateSpike(int tileX, int tileY)
        {
            Spike spike = new Spike(spikeSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            spike.SetPosition(tilePosition);
            spikes.Add(spike);
        }
        // ------------------
        public void Update(GameTime gameTime)
        {
            // Update all actors
            player.Update(gameTime);

            // Collision checking
            List<Wall> collidingWalls = GetTilesInBounds(player.GetBound());
            foreach (Wall collidingWall in collidingWalls)
            {
                player.HandleCollision(collidingWall);
                
            }

            //coin collisions
            foreach( Coin coin in coins)
            {
                if (!reloadLevel && coin.GetVisible() == true && coin.GetBound().Intersects(player.GetBound()))
                    player.HandleCollision(coin);
            }

            //Spike collisions
            foreach (Spike spike in spikes)
            {
                if (!reloadLevel && spike.GetVisible() == true && spike.GetBound().Intersects(player.GetBound()))
                    player.HandleCollision(spike);
            }

            scoreDisplay.SetText("Score: " + player.GetScore());
            LifeDisplay.SetText("Lifes: " + player.GetLives());

            if(reloadLevel == true)
            {
                LoadLevel(currentLevel);
                reloadLevel = false;
            }
        }
        // ------------------
        public List<Wall> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Wall> tilesInBounds = new List<Wall>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / tileWidth);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / tileWidth) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / tileHeight);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / tileHeight) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    // Only add the tile if it exists (is not null)
                    // And if it is visible
                    Wall thisTile = GetTile(x, y);

                    if (thisTile != null && thisTile.GetVisible() == true)
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }
        // ------------------
        public Wall GetTile(int x, int y)
        {
            // Check if we are out of bounds
            if (x < 0 || x >= levelWidth || y < 0 || y >= levelHeight)
                return null;

            // Otherwise we are within the bounds
            return walls[x, y];
        }
        // ------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            
            player.Draw(spriteBatch);

            foreach (Wall eachWall in walls)
            {
                if (!reloadLevel && eachWall != null)
                    eachWall.Draw(spriteBatch);
      
            }

            foreach(Coin coin in coins)
            {
                coin.Draw(spriteBatch);
            }

            foreach (Spike spike in spikes)
            {
                spike.Draw(spriteBatch);
            }

            scoreDisplay.draw(spriteBatch);
            LifeDisplay.draw(spriteBatch);
        }
        // ------------------
        public void LoadLevel(int LevelNum)
        {
            currentLevel = LevelNum;
            string baselevelName = "Levels/level_";
            LoadLevel(baselevelName + LevelNum.ToString()+ ".txt");
        }

        public void LoadLevel(string filename)
        {
            ClearLevel();
            //create filestream toopne file and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(filename);

            //before we read in the individula tiles of our level
            //we need to know how big the level is overall to 
            //create arrays to hold the data

            int lineWidth = 0;//eventually will be level width
            int numLines = 0;
            List<String> lines = new List<string>();
            StreamReader reader = new StreamReader(fileStream);
            string line = reader.ReadLine();
            lineWidth = line.Length;
            while(line != null)
            {
                lines.Add(line);
                    if (line.Length != lineWidth)
                {
                    throw new Exception("lines are different widths - error occured on line" + lines.Count);
                }

                //red the next line to ge ready for the next loop
                line = reader.ReadLine();
                    
            }

            //we have read all the lines into the lines list
            //we can now know how many lines there were
            numLines = lines.Count;

            //now we can set up our tile array
            levelWidth = lineWidth;
            levelHeight = numLines;
            walls = new Wall[levelWidth, levelHeight];

            //loop over every tile position and check every letter there and load base don tile position
            for( int y = 0; y< levelHeight; ++y)
            {
                for (int x=0; x<levelWidth; ++x)
                {
                    //load each tile
                    char tiletype = lines[y][x];
                    //TODO;Load the tile
                    LoadTile(tiletype, x, y);

                }
            }

            //verify that the level is actually playble
            if(player == null)
            {
                //thre new nott supportd exception
                throw new NotSupportedException("a level must having a starting point for the player");
            }
        }

        private void LoadTile(char tileType, int tileX, int tileY )
        {
            switch(tileType)
            {
                //player
                case 'P':
                    PositionPlayer(tileX, tileY);
                    break;

                case 'W':
                    CreateWall(tileX, tileY);
                    break;

                case 'C':
                    CreateCoin(tileX, tileY);
                    break;

                case 'S':
                    CreateSpike(tileX, tileY);
                    break;

                case '.':
                    break;

                default:
                    break;
            }
        }

        public void ResetLevel()
        {
            //delay reloaidng level until after update loop
            reloadLevel = true;
            
        }

        private void ClearLevel()
        {
            spikes.Clear();
            coins.Clear();
        }
    }
}
