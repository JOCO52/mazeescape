﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeEscape
{
    class Coin : Sprite
    {
        //data
        private int scoreValue = 100; 

        // behaviour
        public Coin(Texture2D newTexture) : base(newTexture)//passes the informaiton up to the parent class(base class. Sprite
        {

        }
        /*public Vector2 GetCollisionDepth(Rectangle otherBounds)
        {
            Rectangle tileBounds = GetBound();
            //caclulate the half sizes of bothr rectangles
            float halfwidthPlayer = otherBounds.Width / 2.0f;
            float halfHeightPlayer = otherBounds.Height / 2.0f;
            float halfwidthTile = tileBounds.Width / 2.0f;
            float halfHeightTile = tileBounds.Height / 2.0f;

            //calculate the centers of each rectangle
            Vector2 centrePlayer = new Vector2(otherBounds.Left + halfwidthPlayer, otherBounds.Top + halfHeightPlayer);
            Vector2 centreTile = new Vector2(tileBounds.Left + halfwidthTile, tileBounds.Top + halfHeightTile);


            //distance between the centres
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            //minimum distance these need to be to NOT collide/intersect
            //if either x or the Y distance is gretaer than these minima, these are not itnersecting
            float minDistanceX = halfwidthPlayer + halfwidthTile;
            float mindistanceY = halfHeightPlayer + halfHeightTile;
            }*/

        public int GetCoinValue()
        {
            return scoreValue;
        }
    }

    
}
