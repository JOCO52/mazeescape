﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MazeEscape
{
    class Player : Actor //player inherits from actor
    {
        //------------------------------------
        //Data
        private int score = 0;
        private Level level;
        private int lives = 3;
        
            
        //------------------------------------
        //constants
        private const float MOVE_ACCEL = 100000;
        private const float MOVE_DRAG_FACTOR = 0.4f;
        private const float MAX_MOVE_SPEED = 500;
        private const float MIN_ANIM_SPEED = 500;
        

        //------------------------------------
        //Behaviour
        //------------------------------------
        public Player(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float framesPS, Level newLevel) : base(newTexture,  newFrameWidth,  newFrameHeight, framesPS)
        {
            AddAnimation("WalkDown", 0, 3);
            AddAnimation("Walkright", 4, 7);
            AddAnimation("Walkup", 8, 11);
            AddAnimation("Walkleft", 12, 15);

            playAnimation("WalkDown");

            level = newLevel;
        }

        public override void Update(GameTime gametime)
        {
            float frametime = (float)gametime.ElapsedGameTime.TotalSeconds;

            //get the keyboard state
            KeyboardState keyboardState = Keyboard.GetState();
            //check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;

            if(keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
                playAnimation("Walkleft");
            }

            if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = +1.0f;
                playAnimation("Walkright");
            }

            if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
                playAnimation("Walkup");
            }

            if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = +1.0f;
                playAnimation("WalkDown");
            }

            //add the movment change to the velocity
            Velocity += movementInput * MOVE_ACCEL * frametime;

            //apply drag from the ground
            Velocity *= MOVE_DRAG_FACTOR; //todo remove magic number

            //if the speed is too high clamp it to a reasonable max
            if (Velocity.Length() > MAX_MOVE_SPEED)
            {
                Velocity.Normalize();
                Velocity *= MAX_MOVE_SPEED;

               
                
            }
            //if th player isnt moving stop the animation
            if (Velocity.Length() < MIN_ANIM_SPEED)
            {
                StopAnimation();
            }

            base.Update(gametime);
        }

        public void HandleCollision(Wall HitWall)
        {
            //determine collision depth(With diretion) and magnitude
            //this tells us what direction to move to exit the collision
            Rectangle playerBounds = GetBound();
            Vector2 depth = HitWall.GetCollisionDepth(playerBounds);

            //If the depth is non-zero it means we are colliding with the wall
            if(depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                //resolve the collision along the shallow access, as that is the one we are closer to the dge on therefore it is easier to "squeeze out"
                if (absDepthY < absDepthX)
                {
                    //reslove the colliion on the Y-axis
                    position.Y = position.Y + depth.Y;

                }
                else
                {
                    //reslove the colliion on the Y-axis
                    position.X = position.X + depth.X;

                }
            }
        }

        public void HandleCollision(Coin hitCoin)
        {
            //collect coin

            //hide the coin
            hitCoin.SetVisible(false);

            //TODOadd to score
            score +=  hitCoin.GetCoinValue();
        }

        public void HandleCollision(Spike hitSpike)
        {
            Kill();
            
        }

        public void Kill()
        {
            level.ResetLevel();

            //lose a life
            --lives;
            score = 0;
        }

        public int GetScore()
        {
            return score;
        }

        public int GetLives()
        {
            return lives;
        }
        
    }
}
