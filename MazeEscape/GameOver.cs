﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeEscape
{
    class GameOver
    {
        //-----------------
        //data
        //-------------------
        Text GameOverMessage;

        //-----------------
        //behaviour
        //-----------------

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            //create message
            SpriteFont tempFont = content.Load<SpriteFont>("fonts/largefont");
            GameOverMessage = new Text(tempFont);
            GameOverMessage.SetText("GAME OVER");
            GameOverMessage.SetPosition(new Microsoft.Xna.Framework.Vector2(graphics.Viewport.Bounds.Width / 2, graphics.Viewport.Bounds.Height / 2));
            GameOverMessage.SetAlignment(Text.Alignment.CENTRE);

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameOverMessage.draw(spriteBatch);
        }


    }
}
