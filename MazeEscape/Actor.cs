﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MazeEscape
{
    class Actor : AnimatingSprite
    {
        //-------------
        //data
        //----------------
        protected Vector2 Velocity = new Vector2(100,100);


        //--------------------
        //behaviour
        //--------------------
        public Actor(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float framesPS) : base (newTexture, newFrameWidth,  newFrameHeight, framesPS)//passes the informaitonup to the parent class(base class. Sprite
        {

        }

        public override void Update(GameTime gametime) // override so that it properly replaces with the AnimatingSPrite update function
        {
            float frametime = (float)gametime.ElapsedGameTime.TotalSeconds;

            //update position based on velocity and frame time
            position += Velocity * frametime;

            //make sure our AnimatingSPrite still updates
            base.Update(gametime);
        }
    }
}
